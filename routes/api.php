<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Api\RegisterController@action');
Route::post('login', 'Api\LoginController@action');

//Route::get('profil', 'Api\UserController@profile')->middleware('auth:api');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('test-auth', 'Api\UserController@testAuth');
    Route::get('profil', 'Api\UserController@profile');
});
